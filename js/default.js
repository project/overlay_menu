// start jquery
jQuery(document).ready(function($) {
    var path = '/'+Drupal.settings.overlay_menu_path;
    var menu = Drupal.settings.overlay_menu;
    var panel_html = Drupal.settings.panel_html;
    var overlay_menu_display = Drupal.settings.overlay_menu_display;
    
    $('body').prepend(
        '<div id="overlay-menu-stage"><div id="overlay-menu">\n\
        <div id="overlay-menu-region-top"><img id="overlay-menu-x" src="'+
        path+'/media/close-icon.png"></div>\n\
        <div id="overlay-menu-region-content"></div>\n\
        </div><div id="overlay-menu-icon"><img src="'+
        path+'/media/icon-menu.png"></div></div>');
    
    $('#overlay-menu-icon').click(function(){
        $('div#page').fadeTo('slow', '.1');
        $('div#overlay-menu').fadeIn('slow');
        $('#overlay-menu-icon').fadeOut('slow');
    });
    
    $('#overlay-menu-x').click(function(){
        $('div#page').fadeTo('slow', '1');
        $('div#overlay-menu').fadeOut('slow');
        $('#overlay-menu-icon').fadeIn('slow');
    });
    
    $('#overlay-menu-region-content').append(panel_html);    
    $('div#overlay-menu-stage').css('display', overlay_menu_display);
    
});